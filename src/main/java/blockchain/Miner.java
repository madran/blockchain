package blockchain;

import blockchain.block.Block;
import blockchain.block.BlockGenerator;

public class Miner extends Thread {

    private Blockchain blockchain;
    private BlockGenerator blockGenerator;

    Miner(Blockchain blockchain, BlockGenerator blockGenerator) {

        this.blockchain = blockchain;
        this.blockGenerator = blockGenerator;
    }

    @Override
    public void run() {
        while (blockchain.getNumberOfBlocksToCreate() > 0) {
            Block block = blockGenerator.generateBlock(blockchain.getLastBlock(), blockchain.getNumberOfZeros());
            try {
                blockchain.addBlock(block);
            } catch (IllegalArgumentException e) {
//                System.out.println(e.getStackTrace());
            }
        }
    }
}
