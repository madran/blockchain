package blockchain.block;

import blockchain.util.Hash;

public class CurrentBlock implements Block {

    private long id;
    private long timestamp;
    private Hash hash;
    private Hash prevHash;
    private long buildTime;
    private long miner;

    public CurrentBlock(long id, long timestamp, Hash hash, Hash prevHash, long buildTime) {
        this.id = id;
        this.timestamp = timestamp;
        this.hash = hash;
        this.prevHash = prevHash;
        this.buildTime = buildTime;
    }

    public CurrentBlock(long id, long timestamp, Hash hash, Hash prevHash, long buildTime, long miner) {
        this.id = id;
        this.timestamp = timestamp;
        this.hash = hash;
        this.prevHash = prevHash;
        this.buildTime = buildTime;
        this.miner = miner;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public Hash getHash() {
        return hash;
    }

    @Override
    public Hash getPrevHash() {
        return prevHash;
    }

    @Override
    public long getBuildTime() {
        return buildTime;
    }

    @Override
    public long getMiner() {
        return miner;
    }

    @Override
    public String toString() {

        return
                "Block:" +
                        System.getProperty("line.separator") +
                        "Id: " + id +
                        System.getProperty("line.separator") +
                        "Created by miner # " + getMiner() +
                        System.getProperty("line.separator") +
                        "Timestamp: " + timestamp +
                        System.getProperty("line.separator") +
                        "Magic number: " + hash.getMagicNumber() +
                        System.getProperty("line.separator") +
                        "Hash of the previous block:" +
                        System.getProperty("line.separator") +
                        prevHash +
                        System.getProperty("line.separator") +
                        "Hash of the block:" +
                        System.getProperty("line.separator") +
                        hash +
                        System.getProperty("line.separator") +
                        "Block was generating for " + buildTime + " seconds" +
                        System.getProperty("line.separator");
    }
}
