package blockchain.block;

import blockchain.util.Hash;

public interface Block {

    long getId();

    long getTimestamp();

    Hash getHash();

    Hash getPrevHash();

    long getBuildTime();

    long getMiner();
}
