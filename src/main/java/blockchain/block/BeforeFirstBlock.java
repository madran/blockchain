package blockchain.block;

import blockchain.util.Hash;

public class BeforeFirstBlock implements Block {

    @Override
    public long getId() {
        return 0;
    }

    @Override
    public long getTimestamp() {
        return 0;
    }

    @Override
    public Hash getHash() {
        return new Hash("0");
    }

    @Override
    public Hash getPrevHash() {
        return new Hash("0");
    }

    @Override
    public long getBuildTime() {
        return 0;
    }

    @Override
    public long getMiner() {
        return 0;
    }
}
