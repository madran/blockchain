package blockchain.block;

import blockchain.util.Cryptography;
import blockchain.util.Hash;
import blockchain.util.Timestamp;

public class BlockGenerator {

    private Cryptography cryptography;
    private Timestamp timestamp;

    public BlockGenerator(Cryptography cryptography, Timestamp timestamp) {

        this.cryptography = cryptography;
        this.timestamp = timestamp;
    }

    public Block generateBlock(Block lastBlock, int numberOfZeros) {

        long startTime = timestamp.getTimestamp();

        long headId = lastBlock.getId() + 1L;
        long salt = timestamp.getTimestamp();

        Hash prevHash = lastBlock.getHash();

        Hash hash = cryptography.apply(Long.toString(salt) + headId + prevHash, numberOfZeros);


        long endTime = timestamp.getTimestamp();
        long duration = (endTime - startTime);
        long blockBuildTime = timestamp.toSeconds(duration);

        return new CurrentBlock(
                headId,
                endTime,
                hash,
                prevHash,
                blockBuildTime,
                Thread.currentThread().getId()
        );
    }
}
