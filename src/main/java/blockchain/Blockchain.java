package blockchain;

import blockchain.block.Block;
import blockchain.block.BeforeFirstBlock;
import blockchain.util.Hash;

import java.util.Iterator;

public class Blockchain implements Iterable<Block> {

    private final Hash FIRST_PREV_SHA = new Hash("0");

    private long headId = 0;
    private Node currentNode;
    private Node firstNode;
    private long lenght = 0;
    private int numberOfZeros;
    private long numberOfBlocksToCreate = 0;

    Blockchain(int numberOfZeros) {

        this.numberOfZeros = numberOfZeros;
    }

    Blockchain(int numberOfZeros, int numberOfBlocksToCreate) {

        this.numberOfZeros = numberOfZeros;
        this.numberOfBlocksToCreate = numberOfBlocksToCreate;
    }

    public synchronized void addBlock(Block block) {

        Node node = new Node(block);

        if(!isFirstBlock()) {
            if(!block.getPrevHash().equals(currentNode.block.getHash())) {
                throw new IllegalArgumentException();
            }
        }

        node.setPrevNode(currentNode);

        if (!isFirstBlock()) {
            currentNode.setNextNode(node);
        }

        if (isFirstBlock()) {
            firstNode = node;
        }

        currentNode = node;

        lenght++;
        numberOfBlocksToCreate--;
    }

    public Block getLastBlock() {

        if (currentNode == null) {
            return new BeforeFirstBlock();
        } else {
            return currentNode.getBlock();
        }
    }

    public int getNumberOfZeros() {

        return numberOfZeros;
    }

    public boolean isValid() {

        Hash prevHash = FIRST_PREV_SHA;
        for (Block block : this) {
            if (prevHash.equals(block.getPrevHash())) {
                prevHash = block.getHash();
            } else {
                return false;
            }
        }

        return true;
    }

    public boolean isFirstBlock() {

        return firstNode == null;
    }

    public long getNumberOfBlocksToCreate() {

        return numberOfBlocksToCreate;
    }

    public long lenght() {
        return lenght;
    }

    @Override
    public Iterator<Block> iterator() {
        return new BlockchainIterator(firstNode);
    }

    private class BlockchainIterator implements Iterator<Block> {

        private Node currentNode;

        BlockchainIterator(Node currentNode) {

            this.currentNode = currentNode;
        }

        @Override
        public boolean hasNext() {

            return currentNode != null;
        }

        @Override
        public Block next() {
            Blockchain.this.currentNode = currentNode;
            currentNode = currentNode.getNextNode();
            return Blockchain.this.currentNode.getBlock();
        }
    }

    private class Node {

        Node nextNode;
        Node prevNode;
        Block block;

        Node(Block block) {

            this.block = block;
        }

        public Block getBlock() {
            return block;
        }

        public Node getNextNode() {
            return nextNode;
        }

        public void setNextNode(Node nextNode) {
            this.nextNode = nextNode;
        }

        public Node getPrevNode() {
            return prevNode;
        }

        public void setPrevNode(Node prevNode) {
            this.prevNode = prevNode;
        }
    }
}
