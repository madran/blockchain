package blockchain;

import blockchain.block.Block;
import blockchain.block.BlockGenerator;
import blockchain.util.Sha256;
import blockchain.util.Time;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws InterruptedException {

        Scanner sc = new Scanner(System.in);

        int numberOfZeros = sc.nextInt();

        Blockchain blockchain = new Blockchain(numberOfZeros, 10);

        BlockGenerator blockGenerator = new BlockGenerator(new Sha256(), new Time());

        Miner miner1 = new Miner(blockchain, blockGenerator);
        Miner miner2 = new Miner(blockchain, blockGenerator);
        Miner miner3 = new Miner(blockchain, blockGenerator);

        miner1.start();
        miner2.start();
        miner3.start();
        miner1.join();
        miner2.join();
        miner3.join();

        System.out.println("Blockchain is valid: " + blockchain.isValid());

        for (Block block: blockchain) {
            System.out.println(block);
            System.out.println();
        }
    }
}
