package blockchain.util;

public interface Timestamp {

    long getTimestamp();
    long toSeconds(long timestamp);
}
