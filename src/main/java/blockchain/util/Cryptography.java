package blockchain.util;

public interface Cryptography {

    Hash apply(String input);

    Hash apply(String input, int numberOfZeros);
}
