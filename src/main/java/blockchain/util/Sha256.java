package blockchain.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Random;

public class Sha256 implements Cryptography {

    Random random = new Random();

    public Hash apply(String input) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            /* Applies sha256 to our input */
            byte[] hash = digest.digest(input.getBytes(StandardCharsets.UTF_8));
            StringBuilder hexString = new StringBuilder();
            for (byte elem : hash) {
                String hex = Integer.toHexString(0xff & elem);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return new Hash(hexString.toString());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Hash apply(String input, int numberOfZeros) {

        Hash hash = apply(input);
        int magicNumber = 0;
        while (hash.getHash().indexOf("0".repeat(numberOfZeros)) != 0) {
            magicNumber = random.nextInt();
            hash = apply(hash.getHash() + magicNumber);
        }

        return new Hash(hash.getHash(), String.valueOf(magicNumber));
    }
}

