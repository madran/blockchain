package blockchain.util;

import java.util.Objects;

public class Hash {

    private String hash;
    private String magicNumber;

    public Hash(String hash) {
        this.hash = hash;
        this.magicNumber = "";
    }

    public Hash(String hash, String magicNumber) {
        this.hash = hash;
        this.magicNumber = magicNumber;
    }

    public String getHash() {
        return hash;
    }

    public String getMagicNumber() {
        return magicNumber;
    }

    @Override
    public String toString() {
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hash hash1 = (Hash) o;
        return hash.equals(hash1.hash);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hash);
    }
}
