package blockchain.util;

import java.time.Instant;
import java.util.concurrent.TimeUnit;

public class Time implements Timestamp {

    @Override
    public long getTimestamp() {
        return Instant.now().toEpochMilli();
    }

    @Override
    public long toSeconds(long timestamp) {
        return TimeUnit.MILLISECONDS.toSeconds(timestamp);
    }
}
