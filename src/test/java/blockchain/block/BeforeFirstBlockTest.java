package blockchain.block;

import blockchain.util.Hash;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BeforeFirstBlockTest {

    @Test
    void testGetters() {

        Block block = new BeforeFirstBlock();

        assertEquals(0L, block.getId());
        assertEquals(0L, block.getTimestamp());
        assertEquals(new Hash("0"), block.getPrevHash());
        assertEquals(new Hash("0"), block.getHash());
        assertEquals(0L, block.getBuildTime());
        assertEquals(0L, block.getMiner());
    }
}