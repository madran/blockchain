package blockchain.block;

import blockchain.util.Hash;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CurrentBlockTest {

    @Test
    void testGetters() {

        Block block = new CurrentBlock(1L, 123L, new Hash("1"), new Hash("0"), 456L);

        assertEquals(1L, block.getId());
        assertEquals(123L, block.getTimestamp());
        assertEquals(new Hash("0"), block.getPrevHash());
        assertEquals(new Hash("1"), block.getHash());
        assertEquals(456L, block.getBuildTime());
    }
}