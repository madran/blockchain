package blockchain.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Sha256Test {

    @Test
    void testHashWith5LeadingZeros() {

        Sha256 sha256 = new Sha256();
        Hash hash = sha256.apply("test_input", 5);

        assertTrue(hash.getHash().matches("^0{5}[^0].+"));
    }
}