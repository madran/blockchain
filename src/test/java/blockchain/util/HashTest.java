package blockchain.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HashTest {

    @Test
    void testEquals() {

        Hash hashA1 = new Hash("A");
        Hash hashA2 = new Hash("A");

        assertEquals(hashA1, hashA2);
    }

    @Test
    void testNotEquals() {

        Hash hashA1 = new Hash("A");
        Hash hashB1 = new Hash("B");

        Object object = new Object();

        assertNotEquals(hashA1, hashB1);
        assertNotEquals(hashA1, object);
    }
}