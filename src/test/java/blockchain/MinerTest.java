package blockchain;

import blockchain.block.BlockGenerator;
import blockchain.util.Sha256;
import blockchain.util.Time;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinerTest {

    @Test
    void oneMinerCreate10Blocks() {

        Blockchain blockchain = new Blockchain(2, 10);
        BlockGenerator blockGenerator = new BlockGenerator(new Sha256(), new Time());
        Miner miner = new Miner(blockchain, blockGenerator);
        miner.start();

        try {
            miner.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(10, blockchain.lenght());
    }

    @Test
    void twoMinersCreate10Blocks() {

        Blockchain blockchain = new Blockchain(2, 10);
        BlockGenerator blockGenerator = new BlockGenerator(new Sha256(), new Time());
        Miner minerA = new Miner(blockchain, blockGenerator);
        Miner minerB = new Miner(blockchain, blockGenerator);
        minerA.start();
        minerB.start();

        try {
            minerA.join();
            minerB.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(10, blockchain.lenght());
    }
}