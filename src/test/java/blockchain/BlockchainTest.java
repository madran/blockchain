package blockchain;

import blockchain.block.Block;
import blockchain.block.CurrentBlock;
import blockchain.util.Hash;
import blockchain.util.Sha256;
import blockchain.util.Timestamp;
import org.junit.jupiter.api.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;

class BlockchainTest {

    @Mock
    private Sha256 sha256;

    @Mock
    private Timestamp time;

    @BeforeEach
    void init() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    void addOneBlock() {

        Block newBlock = new CurrentBlock(1L, 123L, new Hash("1"), new Hash("0"), 456L);

        Blockchain blockchain = new Blockchain(5);
        blockchain.addBlock(newBlock);

        for (Block block : blockchain) {
            assertEquals(1L, block.getId());
            assertEquals(123L, block.getTimestamp());
            assertEquals(new Hash("1"), block.getHash());
            assertEquals(new Hash("0"), block.getPrevHash());
            assertEquals(456L, block.getBuildTime());
        }
    }

    @Test
    void addTwoBlocks() {

        Block newBlockA = new CurrentBlock(1L, 123L, new Hash("1"), new Hash("0"), 456L);
        Block newBlockB = new CurrentBlock(2L, 234L, new Hash("2"), new Hash("1"), 567L);

        Blockchain blockchain = new Blockchain(5);
        blockchain.addBlock(newBlockA);
        blockchain.addBlock(newBlockB);

        Block[] blocks = new Block[2];

        int i = 0;
        for (Block block : blockchain) {
            blocks[i++] = block;
        }

        assertEquals(1L, blocks[0].getId());
        assertEquals(123L, blocks[0].getTimestamp());
        assertEquals(new Hash("1"), blocks[0].getHash());
        assertEquals(new Hash("0"), blocks[0].getPrevHash());
        assertEquals(456L, blocks[0].getBuildTime());

        assertEquals(2L, blocks[1].getId());
        assertEquals(234L, blocks[1].getTimestamp());
        assertEquals(new Hash("2"), blocks[1].getHash());
        assertEquals(new Hash("1"), blocks[1].getPrevHash());
        assertEquals(567L, blocks[1].getBuildTime());
    }

    @Test
    void validBlockchainWithZeroBlocks() {

        Blockchain blockchain = new Blockchain(5);

        assertTrue(blockchain.isValid());
    }

    @Test
    void validBlockchainWithOneBlocks() {

        Block newBlock = new CurrentBlock(1L, 123L, new Hash("1"), new Hash("0"), 456L);

        Blockchain blockchain = new Blockchain(5);
        blockchain.addBlock(newBlock);

        assertTrue(blockchain.isValid());
    }

    @Test
    void validBlockchainWithTwoBlocks() {

        Block newBlockA = new CurrentBlock(1L, 123L, new Hash("1"), new Hash("0"), 456L);
        Block newBlockB = new CurrentBlock(2L, 234L, new Hash("2"), new Hash("1"), 567L);

        Blockchain blockchain = new Blockchain(5);
        blockchain.addBlock(newBlockA);
        blockchain.addBlock(newBlockB);

        assertTrue(blockchain.isValid());
    }
    
    @Test
    void invalidBlockchainWithTwoBlocks() {

        Block newBlockA = new CurrentBlock(1L, 123L, new Hash("1"), new Hash("0"), 456L);
        Block newBlockB = new CurrentBlock(2L, 234L, new Hash("2"), new Hash("2"), 567L);

        Blockchain blockchain = new Blockchain(5);
        blockchain.addBlock(newBlockA);

        assertThrows(IllegalArgumentException.class, () -> { blockchain.addBlock(newBlockB); });
    }
}