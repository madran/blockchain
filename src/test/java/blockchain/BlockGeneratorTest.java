package blockchain;

import blockchain.block.Block;
import blockchain.block.BlockGenerator;
import blockchain.util.Hash;
import blockchain.util.Sha256;
import blockchain.util.Timestamp;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BlockGeneratorTest {

    @Mock
    private Sha256 sha256;

    @Mock
    private Timestamp time;

    private Blockchain blockchain;

    private BlockGenerator blockGenerator;

    @BeforeAll
    void beforeAll() {

        MockitoAnnotations.initMocks(this);
        blockGenerator = new BlockGenerator(sha256, time);
    }

    @BeforeEach
    void beforeEach() {

        blockchain = new Blockchain(2);
    }

    @Test
    public void addOneBlock() {

        when(sha256.apply(anyString(), anyInt())).thenReturn(new Hash("sha"));
        when(time.getTimestamp()).thenReturn(1L);

        Block newBlock = blockGenerator.generateBlock(blockchain.getLastBlock(), blockchain.getNumberOfZeros());
        blockchain.addBlock(newBlock);

        for (Block block : blockchain) {
            assertEquals(1, block.getId());
            assertEquals(1, block.getTimestamp());
            assertEquals(new Hash("sha"), block.getHash());
        }
    }

    @Test
    public void addTwoBlocks() {

        when(sha256.apply(anyString(), anyInt()))
                .thenReturn(new Hash("sha1"))
                .thenReturn(new Hash("sha2"));

        when(time.getTimestamp())
                .thenReturn(1L)
                .thenReturn(1L)
                .thenReturn(1L)
                .thenReturn(2L)
                .thenReturn(2L)
                .thenReturn(2L);

        Block newBlockA = blockGenerator.generateBlock(blockchain.getLastBlock(), blockchain.getNumberOfZeros());
        blockchain.addBlock(newBlockA);
        Block newBlockB = blockGenerator.generateBlock(blockchain.getLastBlock(), blockchain.getNumberOfZeros());
        blockchain.addBlock(newBlockB);

        Block[] blocks = new Block[2];

        int i = 0;
        for (Block block : blockchain) {
            blocks[i++] = block;
        }


        assertEquals(1, blocks[0].getId());
        assertEquals(1, blocks[0].getTimestamp());
        assertEquals(new Hash("sha1"), blocks[0].getHash());

        assertEquals(2, blocks[1].getId());
        assertEquals(2, blocks[1].getTimestamp());
        assertEquals(new Hash("sha2"), blocks[1].getHash());
    }
}